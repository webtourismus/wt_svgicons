<?php

namespace Drupal\wt_svgicons;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Class UtilsService.
 */
class SvgIconsHelperService {
  use LoggerChannelTrait;

  public function getSvgIcon($name, $family) {
    $svgPath = '';
    switch ($family) {
      case 'fal':
      case 'light':
        $svgPath = '/svg/fa/light/';
        break;
      case 'far':
      case 'regular':
        $svgPath = '/svg/fa/regular/';
        break;
      case 'fas':
      case 'solid':
        $svgPath = '/svg/fa/solid/';
        break;
      case 'fab':
      case 'brands':
        $svgPath = '/svg/fa/brands/';
        break;
      case 'fad':
      case 'duotone':
        $svgPath = '/svg/fa/duotone/';
        break;
      case 'faw':
      case 'white':
        $svgPath = '/svg/fa/white/';
        break;
      case 'feather':
        $svgPath = '/svg/feather/';
        break;
      case 'wtfrontend':
        $svgPath = '/images/svg/';
        break;
      default:
        break;
    }
    if ($family == 'wtfrontend') {
      $themePath = drupal_get_path('theme', 'wtfrontend');
    } else {
      $themePath = drupal_get_path('module', 'wt_svgicons');
    }
    $iconFile = $themePath . $svgPath . $name . '.svg';
    if (file_exists($iconFile)) {
      $svgSource = file_get_contents($iconFile);
      $svgSource = str_replace(["\r", "\n"], '', trim($svgSource));
      $svgSource = str_replace('  ', ' ', $svgSource);
      $svgSource = preg_replace('/\>\s+\</', '><', $svgSource);
      return $svgSource;
    }
    $this->getLogger('wt_svgicons')->warning("Icon $name or family $family not found");
    return '';
  }
}
