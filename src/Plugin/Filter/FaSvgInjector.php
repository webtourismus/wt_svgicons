<?php

namespace Drupal\wt_svgicons\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\wt_svgicons\SvgIconsHelperService;


/**
 * Provides a filter to help celebrate good times!
 *
 * @Filter(
 *   id = "wt_svgicons",
 *   title = @Translation("Font Awesome SVG Injector"),
 *   description = @Translation("Transforms tags with FontAwesome classes into inlined SVG"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class FaSvgInjector extends FilterBase implements ContainerFactoryPluginInterface {

  /** @var $helper \Drupal\wt_svgicons\SvgIconsHelperService */
  protected $helper;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, SvgIconsHelperService $helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->helper = $helper;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wt_svgicons.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    if (preg_match('/class\s*=\s*"[^"]*(fal|far|fas|fab|fad)[^"]*"/i', $text)) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);
      $xpathSelector = "descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' fal ')] | descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' far ')] | descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' fas ')] | descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' fab ')] | descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' fad ')]";
      $ignoreClasses = [
        'fa-fw',
        'fa-ul',
        'fa-li',
        'fa-xs',
        'fa-sm',
        'fa-lg',
        'fa-1x',
        'fa-2x',
        'fa-3x',
        'fa-4x',
        'fa-5x',
        'fa-6x',
        'fa-7x',
        'fa-8x',
        'fa-9x',
        'fa-10x',
        'fal',
        'far',
        'fas',
        'fab',
        'fad',
        'fa-w-1',
        'fa-w-2',
        'fa-w-3',
        'fa-w-4',
        'fa-w-5',
        'fa-w-6',
        'fa-w-7',
        'fa-w-8',
        'fa-w-9',
        'fa-w-10',
        'fa-w-11',
        'fa-w-12',
        'fa-w-13',
        'fa-w-14',
        'fa-w-15',
        'fa-w-16',
        'fa-w-17',
        'fa-w-18',
        'fa-w-19',
        'fa-w-20',
      ];
      foreach ($xpath->query($xpathSelector) as $element) {
        /** @var \DOMElement $element */
        try {
          $classesStr = $element->getAttribute('class');
          $classes = explode(' ', $classesStr);
          array_filter($classes);
          $family = array_intersect(['fal', 'far', 'fas', 'fab' ,'fad'], $classes);
          $family = reset($family);
          $classes = array_diff($classes, $ignoreClasses);
          foreach ($classes as $class) {
            if (preg_match('/fa-([a-z0-9\-_]+)/', $class, $names)) {
              $icon = $this->helper->getSvgIcon($names[1], $family);
              if ($icon) {
                $displayStyle = array_intersect(['svg--inline', 'svg--fluid', 'svg--responsive', 'svg--dummydisplay'], $classes);
                if (empty($displayStyle)) {
                  $icon = str_replace('class="svg ', 'class="svg svg--inline ', $icon);
                }
                foreach ($classes as $reAdd) {
                  if (!strpos($icon, $reAdd)) {
                    $icon = str_replace('class="', 'class="'. $reAdd . ' ', $icon);
                  }
                }
                $svg = $dom->createDocumentFragment();
                $svg->appendXML($icon);
                $element->parentNode->replaceChild($svg, $element);
                $result->setAttachments(['library' => ['wt_svgicons/core']]);
              }
            }
          }
        }
        catch (\Exception $e) {
          watchdog_exception('wt_fontawesome', $e);
        }

      }
      $result->setProcessedText(Html::serialize($dom));
    }

    return $result;
  }
}
